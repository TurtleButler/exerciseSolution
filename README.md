# Preperation

The [preperation](https://turtlebutler.gitlab.io/Prepare/#/) should already have been done.

# Exercise

1. Clone repo, build code & configure environment:
    * If you haven't already done it:
        * Got to workspace source: `cd ~/catkin_ws/src`
        * Clone: `git clone https://gitlab.com/TurtleButler/exercise.git`
    * Go to workspace root: `cd ~/catkin_ws`
    * Build the code: `catkin_make`
    * Set environment variables by reopening terminal or typing: `source ~/catkin_ws/devel/setup.bash`
    * Connect to our wifi (SSID: Turtlebot_Wifi / Password: turtlebot)
    * Use our ROS master (needed in every new terminal):
        * `export ROS_HOSTNAME=XXX.XXX.XXX.XXX` (your local ip)
        * `export ROS_MASTER_URI=http://XXX.XXX.XXX.XXX:11311` (remote ip)
1. Echo chat topic:
    * Run command `rostopic echo /chat`
    * Wait for published messages → if you see them your workspace works
1. Understand the code
    * Open: `~/catkin_ws/src/exercise/msg/Message.msg` This is the message that will be sent (It contains a sender/username and a content)
    * Open: `~/catkin_ws/src/exercise/CMakeLists.txt` Make file
    * Open: `~/catkin_ws/src/exercise/package.xml` Configures used ros packages and meta information
    * Open: `~/catkin_ws/src/exercise/src/Receiver.cpp` The receiver source file you have to impement
    * Open: `~/catkin_ws/src/exercise/src/Sender.cpp` The sender source file you have to impement
    * If you need help visit the [ROS Wiki](http://wiki.ros.org)
1. Implement chat sender
    * After the program was started it should ask for a username of the sender
    * When the initialization completed publish a message that you have joined the chat
    * Then it should read the message to be sent and publish the message (with sender name and content) to the topic **"chat"**
1. Implement chat receiver
    * The receiver should subscribe the topic **"chat"**
    * It should output every message, that means sender and content, on the console
