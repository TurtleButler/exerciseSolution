#include "ros/ros.h"
#include "exercise/message.h"

const char* topic = "chat";

std::string getInput(const char* info){
	std::cout << info;
	std::string input;
	std::getline(std::cin, input);
	return input;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "sender", ros::init_options::AnonymousName);

	ros::NodeHandle node;

	ros::Publisher pub = node.advertise<exercise::message>(topic, 100);

	std::string username = getInput("What's your username? ");

	exercise::message msg;
	msg.sender = username;
	msg.content = "Has entered the chat!";
	pub.publish(msg);

	while (ros::ok())
	{
		exercise::message msg;
		msg.sender = username;
		msg.content = getInput("Message: ");

		pub.publish(msg);
	}

	return 0;
}