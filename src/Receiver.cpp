#include "ros/ros.h"
#include "exercise/message.h"

const char* topic = "chat";

void chatCallback(const exercise::message::ConstPtr& msg)
{
   std::cout << msg->sender << ": " << msg->content << std::endl;
}

int main(int argc, char **argv)
{
   ros::init(argc, argv, "receiver", ros::init_options::AnonymousName);

   ros::NodeHandle node;

   ros::Subscriber sub = node.subscribe(topic, 100, chatCallback);

   ros::spin();

   return 0;
}